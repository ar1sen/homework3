#ifndef SIMULATION_H_
#define SIMULATION_H_

#include "event.h"
#include "minheap.h"

class Simulation{

public:

	Simulation(int t);
	virtual ~Simulation();

    int getTime();
    int getDuration();
    void scheduleEvent(int time, Event *e);
	void run();

private:

	int time;
	int t;
	MinHeap* kalender;

};
#endif