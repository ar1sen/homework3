#include "mm1_utils.h"

Mm1_utils::Mm1_utils(int a, int m){

	this->a = a;
	this->m = m;
	this->customers = 0;
	this->counter_free = true;
	this->L = 1;	//max. Laenge der Warteschlange
	this->S = 0;	//Anzahl bedienter Kunden
}

int Mm1_utils::getNewCustomerArrivalInterval(){	//Abstand zwischen zwei Kunden

    double u = ((double)rand() / RAND_MAX);
	return -log(1 - u)*a + 0.5; //add 0.5 to round properly
}

int Mm1_utils::getServiceTime(){	//Bearbeitungszeit

    double u = ((double)rand() / RAND_MAX);
	return -log(1 - u)*m + 0.5; //add 0.5 to round properly
}

void Mm1_utils::addNewCustomer(){

    customers++;
	L = (customers > L)? customers:L; // ggf. max. Laenge der Warteschlange setzen
}

void Mm1_utils::serveCustomer(){
    
	if(customers > 0){
        customers--;
		S++; //Anzahl bedienter Kunden erhoehen
	}
}

bool Mm1_utils::getCounterFree(){
	return counter_free;
}

void Mm1_utils::setCounterFree(bool val){
	counter_free = val;
}

int Mm1_utils::getL(){
	return L;
}

int Mm1_utils::getS(){
	return S;
}


