#include "server.h"

Server::Server(Mm1_utils* mm1_utils): Event(){

    this->mm1utils = mm1_utils;
}

Server::~Server(){}

void Server::run(Simulation* s){

    if( mm1utils->getCounterFree() )

        //No customers. Nothing to do.
        mm1utils->setCounterFree(true);

	else{

		mm1utils->setCounterFree(false);
		mm1utils->serveCustomer();

     	//schedule next server event
		int next_service_time = s->getTime() + mm1utils->getServiceTime();

        Server* server = new Server(mm1utils);
        server->setStartTime(next_service_time);
		s->scheduleEvent(next_service_time, server);
	}
}