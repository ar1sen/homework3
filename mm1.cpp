#include "mm1.h"

Mm1::Mm1(int t, int a, int m) : Simulation(t){

    //	this->t = t; hat fehler ausgegeben -> t ist private, deshalb den konstruktor aufrufen
	mm1utils = new Mm1_utils(a, m);
	createEvents();
}

Mm1::~Mm1(){

	delete(mm1utils);
}

void Mm1::print(){

	cout << mm1utils->getL() << "\t" << mm1utils->getS() << endl;
}

void Mm1::createEvents(){

	int customer_start_time = 0;

	do{
		scheduleEvent(customer_start_time, new Generator(mm1utils));
		customer_start_time += mm1utils->getNewCustomerArrivalInterval();
	}
    while(customer_start_time < this->getDuration());
}