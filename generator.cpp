#include "generator.h"

Generator::Generator(Mm1_utils* mm1utils): Event(){

	this->mm1utils = mm1utils;
}

Generator::~Generator(){}

void Generator::run(Simulation* s){

    // Customer can be served immediately
    // Schedule new server event with current time as start time
	if(mm1utils->getCounterFree()){

        Server* server = new Server(mm1utils);
		server->setStartTime(s->getTime());
		s->scheduleEvent(s->getTime(), server);
	}

	mm1utils->addNewCustomer();
}
