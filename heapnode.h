#include "event.h"

#ifndef HEAPNODE_H_
#define HEAPNODE_H_

class HeapNode{
public:

    HeapNode(Event* e, int t);
    ~HeapNode();

    Event* e;
    int t;
    
};
#endif