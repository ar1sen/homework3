CC = g++
CFLAGS=-g -Wall -Wc++0x-compat
OBJ = heapnode.o minheap.o event.o generator.o server.o mm1.o mm1_utils.o simulation.o main.o
TARGET = Aufgabe3

all: $(TARGET)

%.o: %.cpp %.h
	$(CC) $(CFLAGS) -c $<

%.o: %.cpp
	$(CC) $(CFLAGS) -c $<

$(TARGET): $(OBJ)
	$(CC) $(CFLAGS) -o $@ $^

run: all
	./$(TARGET)

clean:
	$(RM) $(TARGET) *.o
