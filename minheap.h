#ifndef MINHEAP_H_
#define MINHEAP_H_

#include <iostream>
#include "heapnode.h"
#include "event.h"

using namespace std;

class MinHeap{
private:

    int m_size;
    int m_capacity;

    HeapNode **buffer;

public:

    MinHeap();
    ~MinHeap();

    int getSize();

    void add(Event *newEvent, int t);
    void Insert(HeapNode *newNode);

    void pop_back();
    void reserveMemory(int capacity);

    Event* getMin();
    void BubbleDown(int index);
    void BubbleUp(int index);
    void Heapify();

    HeapNode **begin();
    HeapNode **end();

    HeapNode* operator[](int index);    
};
#endif
