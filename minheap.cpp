#include "minheap.h"

HeapNode::HeapNode(Event* e, int t){

    this->e = e;
    this->t = t;
}

MinHeap::MinHeap(){
    
    m_capacity = 0;
    m_size = 0;
    buffer = NULL;
}

int MinHeap::getSize(){

    return m_size;
}


void MinHeap::Insert(HeapNode *newNode){

    if (m_size >= m_capacity)
        reserveMemory(m_capacity+10);

    buffer [m_size++] = newNode;
    BubbleUp(getSize()-1);
}

void MinHeap::add(Event *newEvent, int t){

    HeapNode* newNode = new HeapNode(newEvent, t);
    Insert(newNode);
}

void MinHeap::reserveMemory(int newCapacity){

    if(buffer == 0){
        m_size = 0;
        m_capacity = 0;
    }

    HeapNode** Newbuffer = new HeapNode*[newCapacity];

    int newSize = newCapacity < m_size ? newCapacity : m_size;

    for (int i = 0; i < newSize; i++)
        Newbuffer[i] = buffer[i];

    m_capacity = newCapacity;
    delete[] buffer;
    buffer = Newbuffer;
}

HeapNode* MinHeap::operator[](int index){

    return buffer[index];
}

HeapNode** MinHeap::begin(){

    return buffer;
}

HeapNode** MinHeap::end(){

    return buffer+getSize();
}

MinHeap::~MinHeap(){

    delete[] buffer;
}

void MinHeap::pop_back(){

    m_size--;
}


Event* MinHeap::getMin(){

    Event* tmp = buffer[0]->e;

    if ( m_size==0 ){

        cout << "Underflow...\n";
        return tmp;
    }

    buffer[0] = buffer[getSize()-1];
    pop_back();
    BubbleDown(0);
    return tmp;
}

void MinHeap::Heapify(){

    for(int i = m_size-1; 0<=i; --i)
        BubbleDown(i);
}

void MinHeap::BubbleDown(int index){

    int length = m_size;
    int leftChildIndex = 2*index + 1;
    int rightChildIndex = 2*index + 2;

    if( leftChildIndex >= length )
        return;

    int minIndex = index;

    if( buffer[index]->t > buffer[leftChildIndex]->t )
        minIndex = leftChildIndex;

    if( (rightChildIndex < length) && (buffer[minIndex]->t > buffer[rightChildIndex]->t) )
        minIndex = rightChildIndex;

    if( minIndex != index ){

        HeapNode* temp = buffer[index];
        buffer[index] = buffer[minIndex];
        buffer[minIndex] = temp;
        BubbleDown(minIndex);
    }
}

void MinHeap::BubbleUp(int index){

    if( index == 0 )
        return;

    int parentIndex = (index-1)/2;

    if( buffer[parentIndex]->t > buffer[index]->t ){

        HeapNode* tmp = buffer[parentIndex];
        buffer[parentIndex] = buffer[index];
        buffer[index] = tmp;
        BubbleUp(parentIndex);
    }
}