#include "event_vector.h"//wieso???

#ifndef EVENTVECTOR_H__
#define EVENTVECTOR_H__

class EventVector {
private:

    int m_size;
    int m_capacity;
    Event **buffer;
    										// TODO: implement as minheap in array
public:

    EventVector();
    ~EventVector();

    int getSize();
    void add(Event *newEvent, int time);
    void reserveMemory(int capacity);
    Event* operator[](int index);// nicht mehr noetig. stattdessen:
    Event* getMin(); // gibt das oberste Element des Heap zur�ck und loescht es dann sofort

    Event **begin();
    Event **end();
};

#endif
