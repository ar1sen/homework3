#ifndef MM1_UTILS_H_
#define MM1_UTILS_H_

#include <iostream>
#include <math.h>

class Mm1_utils{
private:

    int a;
	int m;
	int customers;//Warteschlange
	bool counter_free;

	int L;	//max. Laenge der Warteschlange
	int S;	//Anzahl bedienter Kunden

public:

    Mm1_utils(int a,int m);
    ~Mm1_utils();

    int getNewCustomerArrivalInterval(); // Zeit zwischen Ankunft zweier Kunden (Exponentialverteilung)
	int getServiceTime();	  // Bearbeitungszeit (Exponentialverteilung)

    bool getCounterFree();
	void setCounterFree(bool val);

    void addNewCustomer();
	void serveCustomer();

    int getL();
	int getS();

};
#endif
