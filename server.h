#ifndef SERVER_H_
#define SERVER_H_

#include "event.h"
#include "mm1_utils.h"

class Server: public Event{
private:

	Mm1_utils* mm1utils;

public:

	Server(Mm1_utils* mm1_utils);
	~Server();

    void run(Simulation* s);
    
};
#endif
