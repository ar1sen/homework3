#include "mm1_utils.h"
#include "event.h"
#include "server.h"

#ifndef GENERATOR_H_
#define GENERATOR_H_

class Generator: public Event{
private:

    Mm1_utils* mm1utils;

public:

    Generator(Mm1_utils* mm1utils);
	~Generator();

    void run(Simulation* s);

};
#endif
