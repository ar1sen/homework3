#include <stdlib.h>
#include "event_vector.h"
#include "event.h"
    										// TODO: implement as minheap in array
EventVector::EventVector(){
    
    m_capacity = 0;
    m_size = 0;
    buffer = NULL;
}

int EventVector::getSize(){
    return m_size;
}

void EventVector::add(Event *newEvent, int time)
{
    if (m_size >= m_capacity)
        reserveMemory(m_capacity+10);

    buffer [m_size++] = newEvent;
}

void EventVector::reserveMemory(int newCapacity)
{
    if(buffer == 0){
        m_size = 0;
        m_capacity = 0;
    }

    Event** Newbuffer = new Event*[newCapacity];
    int newSize = newCapacity < m_size ? newCapacity : m_size;
    for (int i = 0; i < newSize; i++)
        Newbuffer[i] = buffer[i];

    m_capacity = newCapacity;
    delete[] buffer;
    buffer = Newbuffer;
}

Event* EventVector::operator[](int index){
    return buffer[index];
}

Event** EventVector::begin(){
    return buffer;
}

Event** EventVector::end(){
    return buffer+getSize();
}

EventVector::~EventVector(){

    delete[] buffer;
}
