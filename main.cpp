#include <iostream>
#include "mm1.h"

#define SEC 1
#define MIN 60
#define HOUR 3600

using namespace std;

int main(void){

    //Mm1* bank = new Mm1();

    //Oeffnungszeitdauer
    int t = 8 * HOUR;
    cout << "L:\t S:" << endl;

    //mittlere Ankunftszeit
    for(int a=30; a<=300; a+=30)

        //mittlere Bearbeitungszeit
    	for (int m=30; m<=600; m+=30){


            Mm1* bank = new Mm1(t, a, m);
    		bank->run();
    		bank->print();
            delete(bank);
        }

    return 0;
}
