#ifndef MM1_H_
#define MM1_H_

#include <iostream>
#include "mm1_utils.h"
#include "simulation.h"
#include "generator.h"

using namespace std;

class Mm1: public Simulation {
private:

    Mm1_utils* mm1utils;
	void createEvents();

public:

    Mm1(int t, int a, int m);
	virtual ~Mm1();
	void print();

};
#endif
