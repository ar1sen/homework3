#ifndef EVENT_H_
#define EVENT_H_

#include "simulation.h"

class Event{
private:

    int start_time;

public:

    Event();
	virtual ~Event();

    virtual void run(Simulation* s) = 0;
    int getStartTime();
	void setStartTime(int start_time);
    
};
#endif
