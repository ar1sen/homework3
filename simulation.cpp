#include "simulation.h"

Simulation::Simulation(int t){

    kalender = new MinHeap();
	this->t = t;
	time = 0;
}

Simulation::~Simulation(){

    delete kalender;
}

int Simulation::getTime(){

	return time;
}

int Simulation::getDuration(){

    return t;
}

void Simulation::scheduleEvent(int time, Event *e){

    kalender->add(e, time);
}

void Simulation::run(){


    while(time < t){

        Event* currentEvent = kalender->getMin();
		time = currentEvent->getStartTime();
		currentEvent->run(this);

        delete (currentEvent);
	}
}
